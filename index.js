import 'dotenv/config'
import SingleObservable from './Observers/SingleObservable'

const SECRET = process.env.SECRET
console.log('TEST DOT_ENV', { SECRET })

function main() {
  const singleObservable = new SingleObservable()
  singleObservable.trySingleCanGetDataOnNext()
}

main()
